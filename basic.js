const assert = require('assert');
const path = require('path');

describe('first automation practice', () => {

    const mail = 'kek10@example.com';
    const pwd = '1234567890';

    function login() {

        const mailField = $('input[name="email"]');
        const pwdField = $('input[type="password"]');
        const loginBtn = $('button.button.is-primary');

        mailField.setValue(mail);
        pwdField.setValue(pwd);
        loginBtn.click();
    }

    xit('shouldnt have login', () => {
        browser.url('https://staging.bsa-hedonist.online/login');

        const mailField = $('input[name="email"]');
        const loginBtn = $('button.button.is-primary');

        mailField.setValue(mail);
        loginBtn.click();

        $('div.toast').isDisplayed();

    });

    xit('should have sing in', () => {
        browser.url('https://staging.bsa-hedonist.online/login');

        const accBtn = $('a.link.link-signup');
        accBtn.click();
        

          const firstNameField = $('input[name="firstName"]');
          const secondNameField = $('input[name="lastName"]');
          const mailField = $('input[name="email"]');
          const pwdField = $('input[type="password"]');
          const signBtn = $('button.button.is-primary');
       
          firstNameField.setValue('Василий');
          secondNameField.setValue('ФамилияВасилия');
          mailField.setValue(mail);
          pwdField.setValue(pwd);
          browser.pause(2000);
          signBtn.click();
          
          const successExist = $('div.toast.is-success').isDisplayed;

          assert.equal(successExist, true);
                    
     });

    

    xit('should create list ', () => {
        browser.url('https://staging.bsa-hedonist.online/my-lists');

        login();
        $('canvas.mapboxgl-canvas').waitForDisplayed();
        

        const burgerBtn = $('a.navbar-burger');
        const listsBtn = $('a[href="/my-lists"]');
        const profileAct = $('div.profile');

        if (burgerBtn.isDisplayed()) {
            burgerBtn.click();
        } else {
            profileAct.moveTo();
        };
        listsBtn.waitForDisplayed();
        listsBtn.click();

        const createBtn = $('a[href="/my-lists/add"]');
        //createBtn.waitForDisplayed();
        browser.pause(3000);
        createBtn.click();

        browser.pause(3000);

        const listName = 'newlist1'
        const listNameField = $('input#list-name');
        const saveBtn =$('button.button.is-success');

        listNameField.setValue(listName);
        saveBtn.click();


        browser.pause(5000);

        $('=1' + listName).isDisplayed();

    });



    xit('should delete list', () => {
        browser.url('https://staging.bsa-hedonist.online/my-lists');

        login();
        $('canvas.mapboxgl-canvas').waitForDisplayed();
        

        const burgerBtn = $('a.navbar-burger');
        const listsBtn = $('a[href="/my-lists"]');
        const profileAct = $('div.profile');

        if (burgerBtn.isDisplayed()) {
            burgerBtn.click();
        } else {
            profileAct.moveTo();
        };
        listsBtn.waitForDisplayed();
        listsBtn.click();

        const createBtn = $('a[href="/my-lists/add"]');
        //createBtn.waitForDisplayed();
        browser.pause(3000);

        const deleteBtn = $('button.button.is-danger');
        deleteBtn.click();

        browser.pause(1000);

        $('div.buttons button.is-danger').click();


    });

    it('should add new place', () => {
        browser.url('https://staging.bsa-hedonist.online/places/add');

        login();
        //$('canvas.mapboxgl-canvas').waitForDisplayed();
        browser.pause(2500);

        const burgerBtn = $('div.navbar-brand.navbar-brand-name a.navbar-burger');
        const placesBtn = $('a[href="/places/add"]');
        const profileAct = $('div.profile');

        if (burgerBtn.isDisplayed()) {
            burgerBtn.click();
        } else {
            profileAct.moveTo();
        };
        placesBtn.waitForDisplayed();
        placesBtn.click();
        
        const nameField = $('input.input.is-medium');
        const cityField = $$('input.input')[3];
        const zipField = $$('input.input')[4];
        const adressField = $$('input.input')[5]
        const phoneField = $('input[type="tel"]');
        const siteField = $$('input.input')[10];
        const descField = $('textarea.textarea');
        const dropdownBtn = $$('div.dropdown-content')[3];
        const successBtn = $$('div.buttons span.button.is-success');
        

        nameField.setValue('sometext');
        browser.pause(1000);
        cityField.clearValue();
        cityField.setValue('kiev');
        browser.pause(2000);
        dropdownBtn.click();
        zipField.setValue('69000');
        adressField.setValue('sometext');
        phoneField.setValue('+380666666666');
        siteField.setValue('https://example.com');
        descField.setValue('more then twenty symbols desc');

        browser.pause(3000);
        successBtn[0].click();
        
        browser.pause(15000);
        
        
        
        /*
        const path = require('path');
        const filePath = path.join(__dirname, '\asset\fyiLVLo.jpg');
        browser.uploadFile(filePath);
        //$('div.upload-draggable').click();
        //browser.pause(5000);
        //browser.keys(filePath);*/

       /*РАСКОМЕНЬТЬ ЭТОТ КУСОК!!! 
       const uploadControl = $('input[type=file]');
      browser.pause(5000);
      

      const filePath = path.join(__dirname, '5.png');
      const remoteFilePath = browser.uploadFile(filePath);
      uploadControl.setValue(remoteFilePath);

      browser.pause(5000);*/
        
        successBtn[1].click();
        browser.pause(1000);

        successBtn[2].click();
        browser.pause(5000);
        
        const selectCat = $$('div.control span.select')[0];
        const selectTag = $$('div.control span.select')[1];
        
        //selectCat.selectByIndex(1);
        browser.pause(7000);
        //selectTag.selectByIndex(10);
        //browser.pause(5000);

        successBtn[3].click();

        /*$('input[type="checkbox"]').click();
        browser.pause(5000);

        successBtn[4].click();
        successBtn[5].click();

        browser.pause(10000);*/

    });

});